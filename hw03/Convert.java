/* 

Philip Stefanov
Professor Chen
CSE 002
2/12/2019

Program prompts user for a value in meters, and converts
that value to inches

*/

import java.util.Scanner;

public class Convert {
  
  // main method required for every Java program
  
  public static void main(String[] args){
    
    double distanceMeters; // distance in meters
    final double METERS_TO_INCHES = 39.37; // converts cubic inches to cubic miles
    
    Scanner myScanner = new Scanner (System.in);
    
    System.out.println("Enter the distance in meters: "); // prints prompt statement for distance in meters
    distanceMeters = myScanner.nextDouble(); // allows user to enter input for distance in meters
   
    
    System.out.print(distanceMeters * METERS_TO_INCHES+" inches"); // converts to inches
    

    
  } // end of main method
} // end of class