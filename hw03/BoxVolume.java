/* 

Philip Stefanov
Professor Chen
CSE 002
2/12/2019

Program prompts user for the length, width,
and height of a box, and then calculates 
the volume of the box

*/

import java.util.Scanner;

public class BoxVolume {
  
  // main method required for every Java program
  
  public static void main(String[] args){
    
    double length; // length of box
    double width; // width of box
    double height; // height of box
   
    
    Scanner myScanner = new Scanner (System.in);
    
    System.out.println("Enter the length of the box: "); // prints prompt statement for length of box
    length = myScanner.nextDouble(); // allows user to enter input for length of box
    
    System.out.println("Enter the width of the box: "); // prints prompt statement for width of box
    width = myScanner.nextDouble(); // allows user to enter input for width of box
    
    System.out.println("Enter the height of the box: "); // prints prompt statement for height of box
    height = myScanner.nextDouble(); // allows user to enter input for height of box
   
    
    System.out.println("The volume is "+length*width*height); // calculates volume of box
    

    
  } // end of main method
} // end of class