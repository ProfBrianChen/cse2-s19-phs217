/* 

Philip Stefanov
Professor Chen
CSE 002
2/12/2019

Program calculates:

Total cost of each kind of item (i.e. total cost of pants, etc)
Sales tax charged buying all of each kind of item (i.e. sales tax charged on belts)
Total cost of purchases (before tax)
Total sales tax
Total paid for this transaction, including sales tax. 

for pants, sweatshirts, and belts bought. 


*/

public class Arithmetic {
  
  // main method required for every Java program
  
  public static void main(String[] args){
    
    // number of pairs of pants
    
    int numPants = 3;
    
    // cost per pair of pants 
    
    double pantPrice = 34.98;
    
    // number of sweatshirts
    
    int numShirts = 2;
    
    // cost per sweatshirt
    
    double shirtPrice = 24.99;
    
    // number of belts
    
    int numBelts = 1;
    
    // cost per belt
    
    double beltPrice = 33.99;
    
    // the tax rate
    
    double paSalesTax = 0.06;
    
    double totalCostPants; // total cost of pants
    double totalCostShirts; // total cost of shirts
    double totalCostBelts; // total cost of belts
    
    totalCostPants = ((numPants * pantPrice) * 100);// calculates the total cost of buying 3 pairs of pants
    totalCostPants = totalCostPants/100;
    totalCostShirts = ((numShirts * shirtPrice) * 100);// calculates the total cost of buying 2 sweatshirts
    totalCostShirts = totalCostShirts/100;
    totalCostBelts = ((numBelts * beltPrice) * 100);// calculates the total cost of buying 1 belt 
    totalCostBelts = totalCostBelts/100;
    
    double taxPants; // sales tax of buying pants
    double taxShirts; // sales tax of buying shirts
    double taxBelts; // sales tax of buying belts
    
    taxPants = ((totalCostPants * paSalesTax) * 100); // calculates tax on pants
    taxPants = (int)taxPants;
    taxPants = taxPants/100;
    taxShirts = ((totalCostShirts * paSalesTax) * 100); // calculates tax on sweatshirts
    taxShirts = (int)taxShirts;
    taxShirts = taxShirts/100;
    taxBelts = ((totalCostBelts * paSalesTax) * 100); // calculates tax on belts
    taxBelts = (int)taxBelts;
    taxBelts = taxBelts/100;
    
    double totalCost; // total cost of purchases
    
    totalCost = ((totalCostPants + totalCostShirts + totalCostBelts) * 100); // calculates total cost of purchases
    totalCost = totalCost/100;
    
    double taxTotal; // total tax of purchases
    
    taxTotal = ((totalCost * paSalesTax) * 100); // calculates total tax of purchases
    taxTotal = (int) taxTotal;
    taxTotal = taxTotal/100;
    
    double totalCostTaxed; // total cost of purchases with tax
    
    totalCostTaxed = ((totalCost + totalCost * paSalesTax) * 100); // calculates total cost of purchases with tax
    totalCostTaxed = (int)totalCostTaxed;
    totalCostTaxed = totalCostTaxed/100;
    
    System.out.println("The total cost of pants was $"+totalCostPants);
    System.out.println("The total cost of sweatshirts was $"+totalCostShirts);
    System.out.println("The total cost of belts was $"+totalCostBelts);
    
    System.out.println("The sales tax charged on pants was $"+taxPants);
    System.out.println("The sales tax charged on sweatshirts was $"+taxShirts);
    System.out.println("The sales tax charged on belts was $"+taxBelts);
    
    System.out.println("The total cost of purchases was $"+totalCost);
    System.out.println("The total sales tax was $"+taxTotal);
    System.out.println("The total cost of purchases with tax was $"+totalCostTaxed);
    
  }
} //end of main method