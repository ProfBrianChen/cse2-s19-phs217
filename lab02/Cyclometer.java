public class Cyclometer {
  public static void main(String args[]){

      double wheelDiameter = 27.0;
      double PI = 3.14159;
      double feetPerMile = 5280.0;
      double inchesPerFoot = 12.0;
      double secondsPerMinute = 60.0;
      double distanceTrip1, distanceTrip2, totalDistance;
      distanceTrip1 = 0;
      distanceTrip2 = 0;
      totalDistance = 0;
      int secsTrip1 = 480; 
      int secsTrip2 =3220;
      int countsTrip1 = 1561;
      int countsTrip2 = 9037;

      System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");
      System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");

      distanceTrip1 = countsTrip1 * wheelDiameter * PI;
      distanceTrip1 /= inchesPerFoot * feetPerMile;
      distanceTrip2 = countsTrip2 * wheelDiameter * PI;
      distanceTrip2 /= inchesPerFoot * feetPerMile;
      totalDistance = distanceTrip1 + distanceTrip2;

      System.out.println("Trip 1 was "+distanceTrip1+" miles");
      System.out.println("Trip 2 was "+distanceTrip2+" miles");
      System.out.println("The total distance was "+totalDistance+" miles");
    
    
    
    
  } 
}